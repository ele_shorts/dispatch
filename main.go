package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

type (
	dispatch struct {
		listener chan func()
	}
)

func (d *dispatch) Add(f func()) {
	d.listener <- f
}

func (d *dispatch) Run() {
	for {
		select {
		case f := <-d.listener:
			go f()
		}
	}
}
func (d *dispatch) Close() {
	close(d.listener)
}

var (
	port = "9090"
)

func main() {
	s := http.Server{Addr: fmt.Sprintf("0.0.0.0:%s", port)}
	d := dispatch{listener: make(chan func(), 3)}
	defer d.Close()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		t, err := strconv.Atoi(r.URL.Query().Get("t"))
		if err != nil {
			// default
			t = 2
		}
		d.Add(func() {
			time.Sleep(time.Duration(t) * time.Second)
			log.Println("sleep: ", t)
		})
	})

	go d.Run()
	log.Println("listen: ", port)

	if err := s.ListenAndServe(); err != nil {
		log.Fatalln("error: ", err)
	}

}
